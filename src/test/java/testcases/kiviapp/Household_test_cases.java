package testcases.kiviapp;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Household_Page_Objects;
import utilities.ExcelUtitlity_file;

public class Household_test_cases extends Common_Functions {

	@Test(dataProvider = "HouseData")
	public void houseHoldDDT(String periodOfStayhouse, 
			String electricityRegNo, String GasRegNo,
			String noOfCellPhonesAvailable, 
			String lpgPerAnnumNumberAvailable) {
		Household_Page_Objects hp = new Household_Page_Objects();
		hp.setHouseholdfields(periodOfStayhouse, electricityRegNo, 
				GasRegNo, noOfCellPhonesAvailable,
				lpgPerAnnumNumberAvailable);
	}

	@DataProvider(name = "HouseData")
	String[][] getData() throws IOException {
		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\HouseholdtestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] houseHolddata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				houseHolddata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return houseHolddata;
	}
}
	

