package testcases.kiviapp;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Bank_Page_Objects;
import utilities.ExcelUtitlity_file;

public class Bank_test_cases extends Common_Functions {

	@Test(dataProvider = "BankData")
	public void BankDDT(String bankIFSCNo, 
			String bankAccountNoDetails, String confirmBankAccountNoDetails)
			throws AWTException {
		Bank_Page_Objects bp = new Bank_Page_Objects();
		bp.setBankfields(bankIFSCNo, 
				bankAccountNoDetails, confirmBankAccountNoDetails);
	}

	@DataProvider(name = "BankData")
	
	String[][] getData() throws IOException {

		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\BanktestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] bankdata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				bankdata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return bankdata;
	}
}

