package testcases.kiviapp;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Family_Page_objects;
import utilities.ExcelUtitlity_file;

public class Family_test_cases extends Common_Functions {

	@Test(dataProvider = "FamilyData")
	public void familyDDT(String noOfFamilyMember, String noOfDependant, 
			String spouseFnName, String spouseLnName,
			String spouseidProofNo, String spouseDOB1) throws InterruptedException, AWTException {
		Family_Page_objects fp = new Family_Page_objects();
		fp.setFamilyfields(noOfFamilyMember, noOfDependant, 
				spouseFnName, spouseLnName, spouseidProofNo, spouseDOB1);
		Thread.sleep(3000L);
	}

	@DataProvider(name = "FamilyData")
	String[][] getData() throws IOException {
		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\FamilytestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] familydata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				familydata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return familydata;
	}
}
	
	

