package testcases.kiviapp;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Transport_Page_Objects;
import utilities.ExcelUtitlity_file;

public class Transport_test_cases extends Common_Functions {

	@Test(dataProvider = "TransportData")
	public void TransportDDT(String tractorDaysPerYearDetails, 
			String tractorLeasePriceForDayDetails) throws Exception {
		Transport_Page_Objects trp = new Transport_Page_Objects();
		trp.setTransportfields(tractorDaysPerYearDetails, tractorLeasePriceForDayDetails);
	}

	@DataProvider(name = "TransportData")
	String[][] getData() throws IOException {
		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\TransporttestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] transportdata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				transportdata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return transportdata;
	}
}
	
	

