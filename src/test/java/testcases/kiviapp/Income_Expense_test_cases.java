package testcases.kiviapp;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Income_Page_objects;
import utilities.ExcelUtitlity_file;

public class Income_Expense_test_cases extends Common_Functions {

	@Test(dataProvider = "IncomeData")
	public void incomeExpensesDDT(String Areadet, String productivityPerAcre1det, String noOfSeasons1det,
			String avgSalesPrice1det, String inputExpenses1det, String harvestingLabourCost1det, String noOfCattle1det,
			String noOfMilchCattle1det, String avgDailyMilkProduced1det, String soldQuantity1det,
			String pricePerLtr1det, String feedCostPerMonth1det, String groceryExpensesPerMonthdet,
			String utilityBillsdet, String educationalExpensesPerMonthdet, String medicalExpensesPerMonthdet,
			String rentPainPerMonthdet, String habitualExpensesPerMonthdet) {
		
		Income_Page_objects ip = new Income_Page_objects();
		ip.setIncomeExpensesfields(noOfCattle1det, noOfMilchCattle1det,
				avgDailyMilkProduced1det, soldQuantity1det, pricePerLtr1det, feedCostPerMonth1det,
				groceryExpensesPerMonthdet, utilityBillsdet, educationalExpensesPerMonthdet, medicalExpensesPerMonthdet,
				rentPainPerMonthdet, habitualExpensesPerMonthdet);
	}

	@DataProvider(name = "IncomeData")
	String[][] getData() throws IOException {
		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\IncometestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] incomedata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				incomedata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return incomedata;
	}
}
	

