package testcases.kiviapp;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Common_Functions {

	public static Properties properties = null;
	public static WebDriver driver = null;
public static String loggerurl;
	
	
	public Properties loadpropertyfile() throws IOException {
		FileInputStream fileinputstream = new FileInputStream("config.properties");

		properties = new Properties();
		properties.load(fileinputstream);

		return properties;

	}

	//@BeforeSuite
	@BeforeClass
	public void launchBrowser() throws Exception {
	
		//Myscreenrec.startRecording("launchBrowser");
		
		
		loadpropertyfile();
		String browser = properties.getProperty("browser");
		String url = properties.getProperty("url");
		String driverlocation = properties.getProperty("driver_location");
		String chromedriver = properties.getProperty("chrome_driver");
		String firefoxdriver = properties.getProperty("firefox_driver");
  loggerurl = properties.getProperty("loggerURL");
		
		
		
		if (browser.equalsIgnoreCase("chrome")) {
			//System.setProperty(chromedriver, driverlocation);
			//driver = new ChromeDriver();
			String chromeDriverPath = ".//drivers//chromedriver" ;
                System.setProperty("webdriver.chrome.driver", chromeDriverPath);
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors","--disable-extensions","--no-sandbox","--disable-dev-shm-usage");
                options.addArguments("--headless", "--window-size=1920,1200","--ignore-certificate-errors");
                WebDriver driver = new ChromeDriver(options);

		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty(firefoxdriver, driverlocation);
			driver = new ChromeDriver();

		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/kivi-app-root/kivi-welcome-page/div/div/div[3]/div"));

		
		driver.get("https://kiviuat.agrosperity.com/login");
driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("9094823003");
driver.findElement(By.xpath("//button[contains(text(),'Send OTP')]")).click();	
driver.findElement(By.id("otp")).sendKeys("823003");	
driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();	
//Myscreenrec.stopRecording();
	}

	
	 //@AfterSuite
		
	@AfterClass
	public void tearDown1() {

		/*
		 * driver.get("https://kiviuat.agrosperity.com/farmer/list");
		 * driver.findElement(By.xpath(
		 * "//body/kivi-app-root[1]/kivi-fullpage-layout[1]/div[2]/div[2]/div[1]/kivi-onboarded-farmer[1]/div[1]/div[4]/kivi-profile-card[1]/div[1]/div[1]/kivi-edit-farmer-option[1]/div[2]/div[1]/i[1]"
		 * )).click(); driver.findElement(By.xpath(
		 * "//body/kivi-app-root[1]/kivi-fullpage-layout[1]/div[2]/div[2]/div[1]/kivi-onboarded-farmer[1]/div[1]/div[4]/kivi-profile-card[1]/div[1]/div[1]/kivi-edit-farmer-option[1]/div[2]/div[2]/div[1]/div[1]/a[1]"
		 * )).click();
		 * driver.findElement(By.xpath("//button[@id='myDropdown']")).click();
		 * driver.findElement(By.xpath("//button[@id='myDropdown']")).click();
		 * driver.findElement(By.xpath("//button[contains(text(),'Insurance Nominee')]")
		 * ).click();
		 */		
		
		
		/*
		 * driver.findElement(By.xpath("//strong[contains(text(),'Harish User TEST')]"))
		 * .click();
		 * driver.findElement(By.xpath("//button[contains(text(),'Logout')]")).click();
		 */
driver.close();
		//driver.quit();
	}

	
	// public void tearDown() {
	// driver.quit();

	// }

	public static void explicitMethodwaitclick(WebElement waitname) {
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.elementToBeClickable(waitname));
	}

	public static void explicitMethodwait(WebElement waitname1) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOf(waitname1));
	}

	/*
	 * public void cleartimeMethod() {
	 * 
	 * }
	 * 
	 */

	public static void windowScrollmethodupevent() {
	JavascriptExecutor exceutor = (JavascriptExecutor) driver;
	exceutor.executeScript("window.scroll(0,-450)", "");
	
}

	public static void windowScrollmethoddownevent() {
		JavascriptExecutor exceutor1 = (JavascriptExecutor) driver;
		exceutor1.executeScript("window.scrollBy(0,350)", "");
			}
	
	public static void windowScrollmethodtopevent() {
		JavascriptExecutor exceutor = (JavascriptExecutor) driver;
		exceutor.executeScript("window.scroll(0,0)", "");
		
	}
	
}


	
	
	

