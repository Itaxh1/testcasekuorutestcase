package testcases.kiviapp;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Apply_Loan_Page_Objects;
import utilities.ExcelUtitlity_file;

public class Apply_Loan_test_cases extends Common_Functions {

	@Test(dataProvider = "ApplyForLoandata")
	public void applyLoanDDT(String grouploan1, String grouploan2) throws InterruptedException {
		Apply_Loan_Page_Objects ap = new Apply_Loan_Page_Objects();
		ap.setapplyLoanfields(grouploan1, grouploan2);
		Thread.sleep(3000);
	}

	@DataProvider(name = "ApplyForLoandata")
	String[][] getData() throws IOException {
		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\ApplyloantestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] applyLoandata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				applyLoandata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return applyLoandata;
	}
}	
	
	
	
	

