package testcases.kiviapp;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageobjects.kiviapp.Insurance_Page_Objetcs;
import utilities.ExcelUtitlity_file;

public class Insurance_test_cases extends Common_Functions {

	@Test(dataProvider = "InsuranceData")
	public void InsuranceDDT(String nomineeNameDetails, String nomineeDOBDetails) {
		Insurance_Page_Objetcs ip = new Insurance_Page_Objetcs();
		ip.setInsurancefields(nomineeNameDetails, nomineeDOBDetails);
	}

	@DataProvider(name = "InsuranceData")
	String[][] getData() throws IOException {
		String path = "C:\\Users\\HARISH\\eclipse-workspace\\kiviapp2111\\src\\test\\java\\testdata\\kiviapp\\InsurancetestData.xlsx";
		int rownum = ExcelUtitlity_file.getRowCount(path, "Sheet1");
		int colcount = ExcelUtitlity_file.getCellCount(path, "Sheet1", 1);
		String[][] insurancedata = new String[rownum][colcount];
		System.out.println(rownum);

		for (int i = 1; i <= rownum; ++i) {
			for (int j = 0; j < colcount; ++j) {
				insurancedata[i - 1][j] = ExcelUtitlity_file.getCellData(path, "Sheet1", i, j);
			}
		}

		return insurancedata;
	}
}	
	

