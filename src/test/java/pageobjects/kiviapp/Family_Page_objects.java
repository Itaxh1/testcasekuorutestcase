package pageobjects.kiviapp;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Family_Page_objects extends Common_Functions {
	
	
	@FindBy(xpath = "//*[@id=\"maritalStatusForm\"]/div/select")
	public static WebElement maritalStatus;
	
	@FindBy(id = "noOfFamilyMembers")
	public static WebElement noOfFamilyMembers;
	
	@FindBy(id = "noOfDependants")
	public static WebElement noOfDependants;
	
	@FindBy(id = "spousefirstName")
	public static WebElement spouseFirstName;
	
	@FindBy(id = "spouselastName")
	public static WebElement spouseLastName;
	
	@FindBy(xpath = "//*[@id=\"spouseidProofForm\"]/div/select")
	public static WebElement spouseIdProof;
	
	@FindBy(id = "spouseidProofNo")
	public static WebElement spouseIdProofNo;
	
	@FindBy(name = "spousedob")
	public static WebElement spouseDOB;
	
	@FindBy(xpath = "//*[@id='spouseeducationalQualificationForm']/div/select")
	public static WebElement spouseEducationalQualification;
	
	@FindBy(id = "spousecitizenship Indian")
	public static WebElement spouseCitizenship;
	
	@FindBy(id = "Spouse Upload Photo upload")
	public static WebElement spouseUploadPhotoupload;
	
	@FindBy(id = "Spouse Upload Doc upload")
	public static WebElement spouseUploadDocupload;
	
	@FindBy(xpath = "//*[@id='spousereligionForm']/div/select")
	public static WebElement spouseReligion;

	@FindBy(xpath = "//*[@id='spousecasteForm']/div/select")
	public static WebElement spouseCaste;
	
	@FindBy(xpath = "//*[@id='relationship-1Form']/div/select")
	public static WebElement FamilyMember1;
	
	@FindBy(xpath = "//*[@id='relationship-2Form']/div/select")
	public static WebElement FamilyMember2;
	
	@FindBy(xpath = "//*[@id='ngb-panel-3-header']/button")
	public static WebElement spouseButton;
	
	@FindBy(xpath = "//*[@id='ngb-panel-4-header']/button")
	public static WebElement FamilyMember1Click;
	
	@FindBy(xpath = "//*[@id='ngb-panel-5-header']/button")
	public static WebElement FamilyMember2Click;
	
	@FindBy(xpath = "//button[normalize-space()='Save and Continue']")
	public static WebElement saveButtonClick;

	public Family_Page_objects() {
		PageFactory.initElements(driver, this);
	}

	public void setFamilyfields(String noOfFamilyMember, String noOfDependant,
			String spouseFnName, String spouseLnName,
			String spouseidProofNo, String spouseDOB1) throws InterruptedException {
		
		
		Thread.sleep(7000);
		Select select6 = new Select(maritalStatus);
		select6.selectByValue("Married");
		
		noOfFamilyMembers.sendKeys(noOfFamilyMember);
		noOfDependants.sendKeys(noOfDependant);
		
		FamilyMember1Click.click();
		
		Select select11 = new Select(FamilyMember1);
		select11.selectByValue("Father");
		
		FamilyMember1Click.click();
		
		FamilyMember2Click.click();
		
		Select select12 = new Select(FamilyMember2);
		select12.selectByValue("Mother");
		
		FamilyMember2Click.click();
		
		windowScrollmethodtopevent();
		
		spouseButton.click();
		
		spouseFirstName.sendKeys(spouseFnName);
		
		spouseLastName.sendKeys(spouseLnName);
		Select select7 = new Select(spouseIdProof);
		select7.selectByValue("PAN");
		
		spouseIdProofNo.sendKeys(spouseidProofNo);
		spouseDOB.sendKeys(spouseDOB1 + Keys.ENTER);
		
		Select select8 = new Select(spouseEducationalQualification);
		select8.selectByVisibleText("Graduate and Above");
		spouseCitizenship.click();
		
		Select select9 = new Select(spouseReligion);
		select9.selectByVisibleText("Hinduism");
		
		Select select10 = new Select(spouseCaste);
		select10.selectByValue("GEN");

		try {
			saveButtonClick.click();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

