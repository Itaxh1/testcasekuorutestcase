package pageobjects.kiviapp;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Household_Page_Objects extends Common_Functions  {

	@FindBy(id = "periodOfStay")
	public static WebElement periodOfStayinPlace;
	
	@FindBy(id = "typeOfResidence Own")
	public static WebElement TypeofResidence;
	
	@FindBy(xpath = "//*[@id='houseBuildForm']/div/select")
	public static WebElement houseBuildForm;
	
	@FindBy(xpath = "//*[@id='facilitiesAvailableForm']/div/div[1]/label")
	public static WebElement facilitiesAvailableFormTarRoadAccess;
	
	@FindBy(id = "electricityNo")
	public static WebElement electricityNo;
	
	@FindBy(id = "gasConsumerNo")
	public static WebElement gasConsumerNo;
	
	@FindBy(id = "noOfCellPhones")
	public static WebElement noOfCellPhones;
	
	@FindBy(id = "lpgPerAnnumNumber")
	public static WebElement lpgPerAnnumNumber;
	
	@FindBy(xpath = "//*[@id=\"submitForm\"]/div/div[3]/button")
	public static WebElement householdsaveButton;
	
	@FindBy(xpath = "//*[@id='Household Upload Photo upload']")
	public static WebElement HouseholdUploadPhotoupload;
	
	@FindBy(id = "Household Upload Doc upload")
	public static WebElement HouseholdUploadDocupload;

	public Household_Page_Objects() {
		PageFactory.initElements(driver, this);
	}

	public void setHouseholdfields(String periodOfStayhouse, 
			String electricityRegNo, String GasRegNo,
			String noOfCellPhonesAvailable, String lpgPerAnnumNumberAvailable) {
		
		explicitMethodwait(periodOfStayinPlace);
		
		periodOfStayinPlace.sendKeys(periodOfStayhouse);
		
		TypeofResidence.click();
		
		Select select13 = new Select(houseBuildForm);
		select13.selectByValue("Brick");
		
		facilitiesAvailableFormTarRoadAccess.click();
		
		electricityNo.sendKeys(electricityRegNo);
		
		gasConsumerNo.sendKeys(GasRegNo);
		
		noOfCellPhones.sendKeys(noOfCellPhonesAvailable);
		
		lpgPerAnnumNumber.sendKeys(lpgPerAnnumNumberAvailable);

		try {
			String file23 = "C:\\Users\\HARISH\\Desktop\\value.jpg";
			HouseholdUploadPhotoupload.sendKeys(file23);
			
			windowScrollmethoddownevent();
			
			Actions actions = new Actions(driver);
			explicitMethodwaitclick(householdsaveButton);
			
			actions.moveToElement(householdsaveButton).click().perform();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
	

