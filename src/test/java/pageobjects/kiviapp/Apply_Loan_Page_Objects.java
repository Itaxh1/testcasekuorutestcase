package pageobjects.kiviapp;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Apply_Loan_Page_Objects extends Common_Functions {

	@FindBy(id = "Documents Upload Pic upload")
	public static WebElement DocumentsUploadPicupload;
	
	@FindBy(id = "Documents Upload Doc upload")
	public static WebElement DocumentsUploadDocupload;
	
	@FindBy(xpath = "/html/body/kivi-app-root/kivi-fullpage-layout/div[2]/div[2]/div/kivi-onboarding/div/div/div[4]/div/button")
	public static WebElement applyloanButton;
	
	@FindBy(xpath = "//*[@id=\"CroptypeForm\"]/div/select")
	public static WebElement CroptypeForm;
	
	@FindBy(xpath = "//*[@id=\"submitForm\"]/div/div[3]/button")
	public static WebElement continueButton;
	
	@FindBy(id = "grouploan")
	public static WebElement entergrouploan;
	
	@FindBy(xpath = "//*[@id=\"phoneNumberForm\"]/div/div[3]/a")
	public static WebElement enterphoneNumberForm;
	
	@FindBy(xpath = "/html/body/ngb-modal-window/div/div/kivi-consent-verification-modal/div[2]/div/div[2]/input")
	public static WebElement enterphoneNumberotp;
	
	@FindBy(xpath = "/html/body/ngb-modal-window/div/div/kivi-consent-verification-modal/div[2]/div/div[3]/div/button")
	public static WebElement verifyphoneNumberotp;
	
	@FindBy(xpath = "//*[@id=\"submit2Form\"]/div/div[3]/button")
	public static WebElement proceedButton;
	
	@FindBy(xpath = "//*[@id=\"applyforloanForm\"]/div/div[3]/button")
	public static WebElement applyforloanFormfinalButton;

	public Apply_Loan_Page_Objects() {
		PageFactory.initElements(driver, this);
	}

	public void setapplyLoanfields(String grouploan1, String grouploan2) {
		
		applyloanButton.click();
		
		Select select20 = new Select(CroptypeForm);
		select20.selectByValue("Paddy");
		
		continueButton.click();
		entergrouploan.sendKeys(grouploan1);
		
		continueButton.click();
		enterphoneNumberForm.click();
		enterphoneNumberotp.sendKeys("7766");
		
		verifyphoneNumberotp.click();
		proceedButton.click();
		
		entergrouploan.sendKeys(grouploan2);
		applyforloanFormfinalButton.click();
	}
}

