package pageobjects.kiviapp;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Land_Page_Objects extends Common_Functions  {	

	@FindBy(name = "landHoldings")
	public static WebElement landHoldings;
	
	@FindBy(id = "areaUnderFarming")
	public static WebElement areaUnderFarming;
	
	@FindBy(id = "isIrrigatedLand true")
	public static WebElement IrrigatedLand;
	
	@FindBy(id = "isSoilTested true")
	public static WebElement soildTested;
	
	@FindBy(xpath = "//*[@id='irrigationTypeForm']/div/select")
	public static WebElement irrigationTypeForm;
	
	@FindBy(id = "soilTestingAgency")
	public static WebElement soilTestingAgency;
	
	@FindBy(id = "Land Upload Photo upload")
	public static WebElement LandUploadPhotoupload;
	
	@FindBy(id = "Land Upload Doc upload")
	public static WebElement LandUploadDocupload;
	
	@FindBy(xpath = "//button[normalize-space()='Save and Continue']")
	public static WebElement landsaveButtonClick;

	public Land_Page_Objects() {
		PageFactory.initElements(driver, this);
	}

	public void setLandfields(String landHoldingsDetails, 
			String soilTestingAgencyDetails,
			String areaUnderFarmingDetails) {
		
		landHoldings.sendKeys(landHoldingsDetails);
		
		soilTestingAgency.sendKeys(soilTestingAgencyDetails);
		
		areaUnderFarming.sendKeys(areaUnderFarmingDetails);
		
		IrrigatedLand.click();
		
		soildTested.click();
		
		Select select18 = new Select(irrigationTypeForm);
		select18.selectByValue("drop");

		try {
			String file20 = "C:\\Users\\HARISH\\Desktop\\value.jpg";
			LandUploadPhotoupload.sendKeys(file20);
			
			String file21 = "C:\\Users\\HARISH\\Desktop\\value.jpg";
			
			LandUploadDocupload.sendKeys(file21);
		} catch (Exception e) {
			e.printStackTrace();
		}

		landsaveButtonClick.click();
	}
}

