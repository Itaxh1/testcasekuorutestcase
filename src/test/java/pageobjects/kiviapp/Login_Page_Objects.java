package pageobjects.kiviapp;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import testcases.kiviapp.Common_Functions;

public class Login_Page_Objects extends Common_Functions{

	public Login_Page_Objects() {

		PageFactory.initElements(driver, this);
	}
	@FindBy(id = "firstName")
	public static WebElement firstName;

	@FindBy(id = "middleName")
	public static WebElement middleName;

	@FindBy(id = "lastName")
	public static WebElement lastName;

	@FindBy(partialLinkText = "Add Phone Number")
	public static WebElement addPhoneNo;

	@FindBy(id = "fpcShareholder false")
	public static WebElement ShareholderofFPC;

	@FindBy(id = "isInterestedFpc false")
	public static WebElement becomeshareholder;

	@FindBy(xpath = "/html/body/ngb-modal-window/div/div/kivi-mobile-verification-modal/div[2]/div/div[1]/div/input")
	public static WebElement phoneNOfield;

	@FindBy(xpath = "/html/body/ngb-modal-window/div/div/kivi-mobile-verification-modal/div[2]/div/div[2]/div/button")
	public static WebElement sendOTP1;

	@FindBy(xpath = "/html/body/ngb-modal-window/div/div/kivi-mobile-verification-modal/div[2]/div/div[2]/input")
	public static WebElement EnterOTP1;

	@FindBy(xpath = "//*[@class='btn kivi-btn-primary border rounded-0']")
	public static WebElement verifyOTP1;

	@FindBy(id = "pinCode")
	public static WebElement pinCode;

	@FindBy(id = "district")
	public static WebElement PDistrict;

	@FindBy(id = "state")
	public static WebElement PState;

	@FindBy(id = "email")
	public static WebElement emailBox;

	@FindBy(id = "gender Male")
	public static WebElement Gender;

	@FindBy(xpath = "//*[@id=\"dobForm\"]/div/div[1]/div/input")
	public static WebElement DOB;

	@FindBy(xpath = "//*[@id=\"submitForm\"]/div/div[3]/button")
	public static WebElement OnBoardFarmer;

	@FindBy(xpath = "//span[contains(text(),'Basic Details')]")
	public static WebElement BasicDetails;

	@FindBy(xpath = "//*[@id=\"myDropdown\"]")
	public static WebElement generaldropdown;

	@FindBy(xpath = "//body/kivi-app-root[1]/kivi-fullpage-layout[1]/div[2]/div[2]/div[1]/kivi-onboarding[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]")
	public static WebElement Pdetails;

	// Address

	@FindBy(xpath = "//*[@id=\"documentForm\"]/div/kivi-documents/div/div/div[1]/div/div/input")
	public static WebElement addressProofdoc;

	@FindBy(id = "address.pinCode")
	public static WebElement addressPinCode;

	@FindBy(id = "address.block")
	public static WebElement addressBlock;

	@FindBy(id = "address.village")
	public static WebElement addressVillage;

	@FindBy(id = "address.district")
	public static WebElement addressDistrict;

	@FindBy(id = "address.state")
	public static WebElement addressState;

	@FindBy(id = "address.addressOne")
	public static WebElement addressAddressOne;

	@FindBy(id = "address.addressTwo")
	public static WebElement addressAddressTwo;

	@FindBy(id = "address.addressThree")
	public static WebElement addressAddressThree;

	@FindBy(id = "address.landmark")
	public static WebElement addressLandmark;

	@FindBy(xpath = "//*[@id=\"address.idProofForm\"]/div/select")
	public static WebElement addressSelect;

	@FindBy(id = "address.idNumber")
	public static WebElement addressIdNumber;

	@FindBy(id = "communicationAddress sameAsPermanent")
	public static WebElement sameAsPermanent;

	// Other Details

	@FindBy(id = "pan")
	public static WebElement panCardno;

	@FindBy(id = "gstin")
	public static WebElement GSTno;

	@FindBy(xpath = "//*[@id=\'physicalChallengeForm\']/div/select")
	public static WebElement physicalStatus;

	@FindBy(xpath = "//*[@id=\'educationalQualificationForm\']/div/select")
	public static WebElement EduQal;

	@FindBy(id = "citizenship Indian")
	public static WebElement citizenship;

	@FindBy(xpath = "//*[@id=\'religionForm\']/div/select")
	public static WebElement Religion;

	@FindBy(xpath = "//*[@id=\'casteForm\']/div/select")
	public static WebElement caste;

	@FindBy(xpath = "//*[@id=\'idProofForm\']/div/select")
	public static WebElement proofForm;

	@FindBy(xpath = "//*[@id=\"documentForm\"]/div/kivi-documents/div/div/div[1]/div/div/input")
	public static WebElement othersproofForm;

	// span[contains(text(),'Addresses')]
	@FindBy(xpath = "//span[contains(text(),'Addresses')]")
	public static WebElement Addresses;

	@FindBy(id = "Personal Farmer`s Pic upload")
	public static WebElement farmerPIC;

	/*
	 * @FindBy(xpath = "//div[@class='accordion-item']/following::span[2]")
	 */

	@FindBy(xpath = "//*[@id='ngb-panel-2-header']/button")
	public static WebElement Others;

	@FindBy(xpath = "//*[@id=\"ngb-panel-0-header\"]/button")
	public static WebElement basicDetails;

	@FindBy(xpath = "//*[@id=\"myDropdown\"]")
	public static WebElement personalDetails;


	JSONObject jo = new JSONObject();
	

	public void setfields(String fname, String lastname, String phoneNo, String Picode, String district, String state,
			String Email, String DOB1, String addressPiCode, String addressBlk, String addressVillages,
			String addressDistrict1, String addressState1, String addAddressOne, String addAddressTwo,
			String addAddressThree, String addLandmark, String addaddressIdNumber, String panCardNo)
			throws AWTException, InterruptedException {

		/*
		 * farmerPIC.click();
		 * 
		 * 
		 * explicitMethodwaitclick(farmerPIC); farmerPIC.click();
		 * 
		 * String file12 ="C:\\Users\\HARISH\\Desktop\\value.jpg";
		 * 
		 * 
		 * 
		 * // explicitMethodwait(Personal_details_Page_Objects.farmerPIC);
		 * 
		 * Thread.sleep(9000);
		 * 
		 * StringSelection selection7 = new StringSelection(file12);
		 * 
		 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection7,null)
		 * ;
		 * 
		 * 
		 * 
		 * Robot robot15 = new Robot(); // explicitMethodwait((WebElement) robot15);
		 * 
		 * robot15.setAutoDelay(2000);
		 * 
		 * // robot15.keyPress(KeyEvent.VK_PAGE_DOWN);
		 * 
		 * 
		 * // explicitMethodwait((WebElement) selection7);
		 * 
		 * 
		 * robot15.keyPress(KeyEvent.VK_CONTROL); robot15.keyPress(KeyEvent.VK_V);
		 * robot15.keyRelease(KeyEvent.VK_V); robot15.keyRelease(KeyEvent.VK_CONTROL);
		 * 
		 * robot15.keyPress(KeyEvent.VK_ENTER); robot15.keyRelease(KeyEvent.VK_ENTER);
		 */

		// firstName.clear();

		firstName.sendKeys(fname);
		
	jo.put("First_Name", fname);
		System.out.println("Entered the first name");
//		Assert.assertequals(fname,)
		// ss.put("First_Name",fname);
		/*
		 * SoftAssert softAssert = new SoftAssert(); softAssert.assertEquals(fname,
		 * fname);
		 */

		// middleName.sendKeys(midname);
		// ss.put("Middle_Name",midname);

		lastName.sendKeys(lastname);
		jo.put("Last_Name", lastname);
		audit("Kivi_app", jo);
		// ss.put("Last_Name", lastname);
		// audit("testLog-kivi-api", ss);
		addPhoneNo.click();
		phoneNOfield.sendKeys(phoneNo);
		
		
		explicitMethodwait(sendOTP1);
		sendOTP1.click();
		explicitMethodwait(EnterOTP1);
		EnterOTP1.sendKeys("956745");
		explicitMethodwait(verifyOTP1);
		verifyOTP1.click();

		// explicitMethodwait(pinCode);
		pinCode.sendKeys(Picode);
		PDistrict.sendKeys(district);
		PState.sendKeys(state);
		/*
		 * Robot robot = new Robot(); robot.keyPress(KeyEvent.VK_ENTER);
		 * robot.keyRelease(KeyEvent.VK_ENTER);
		 */
		/*
		 * district.sendKeys(Dist); State.sendKeys(States);
		 */
		emailBox.sendKeys(Email);
		DOB.sendKeys(DOB1 + Keys.ENTER);

		Gender.click();

		/*
		 * windowScrollmethodupevent(); explicitMethodwait(farmerPIC);
		 * 
		 * String file12 ="C:\\Users\\HARISH\\Desktop\\value.jpg";
		 * 
		 * farmerPIC.sendKeys(file12);
		 */
		// Thread.sleep(5000);

		// windowScrollmethodtopevent();
		/*
		 * Robot robot14 = new Robot(); robot14.keyPress(KeyEvent.VK_PAGE_UP);
		 * 
		 * robot14.keyRelease(KeyEvent.VK_PAGE_UP);
		 * 
		 * Actions actions = new Actions(driver);
		 * 
		 * actions.moveToElement(farmerPIC); actions.build().perform();
		 */
		/*
		 * farmerPIC.click();
		 * 
		 * 
		 * explicitMethodwaitclick(farmerPIC); farmerPIC.click();
		 * 
		 * String file12 ="C:\\Users\\HARISH\\Desktop\\value.jpg";
		 * 
		 * 
		 * 
		 * // explicitMethodwait(Personal_details_Page_Objects.farmerPIC);
		 * 
		 * Thread.sleep(9000);
		 * 
		 * StringSelection selection7 = new StringSelection(file12);
		 * 
		 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection7,null)
		 * ;
		 * 
		 * 
		 * 
		 * Robot robot15 = new Robot(); // explicitMethodwait((WebElement) robot15);
		 * 
		 * robot15.setAutoDelay(2000);
		 * 
		 * // robot15.keyPress(KeyEvent.VK_PAGE_DOWN);
		 * 
		 * 
		 * // explicitMethodwait((WebElement) selection7);
		 * 
		 * 
		 * robot15.keyPress(KeyEvent.VK_CONTROL); robot15.keyPress(KeyEvent.VK_V);
		 * robot15.keyRelease(KeyEvent.VK_V); robot15.keyRelease(KeyEvent.VK_CONTROL);
		 * 
		 * robot15.keyPress(KeyEvent.VK_ENTER); robot15.keyRelease(KeyEvent.VK_ENTER);
		 * 
		 * 
		 */ OnBoardFarmer.click();
		windowScrollmethodupevent();
		generaldropdown.click();
		Pdetails.click();
		Addresses.click();

		addressPinCode.sendKeys(addressPiCode);
		Robot robot1 = new Robot();
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);

		addressBlock.sendKeys(addressBlk);
		addressVillage.sendKeys(addressVillages);

		addressDistrict.sendKeys(addressDistrict1);
		addressState.sendKeys(addressState1);

		addressAddressOne.sendKeys(addAddressOne);
		addressAddressTwo.sendKeys(addAddressTwo);
		addressAddressThree.sendKeys(addAddressThree);

		addressLandmark.sendKeys(addLandmark); //

		Select select = new Select(addressSelect);
		select.selectByValue("UID");

		addressIdNumber.sendKeys(addaddressIdNumber);
		sameAsPermanent.click();

		windowScrollmethodupevent();
		explicitMethodwait(Addresses);

		Addresses.click();

		windowScrollmethoddownevent();
		explicitMethodwait(Others);
		Others.click();
		panCardno.sendKeys(panCardNo);

		Select select1 = new Select(physicalStatus);
		select1.selectByValue("None");

		Select select2 = new Select(EduQal);
		select2.selectByValue("_10thStd");

		citizenship.click();
		Select select3 = new Select(Religion);
		select3.selectByValue("Hinduism");

		Select select4 = new Select(caste);
		select4.selectByValue("GEN");

		// Personal_details_Page_Objects.proofForm.sendKeys("600118");

		Select select5 = new Select(proofForm);
		select5.selectByValue("PAN");
		System.out.println("user selected  the PAN card");

		try {

			windowScrollmethodupevent();
			Others.click();
			/*
			 * Actions actions = new Actions(driver);
			 * actions.click().moveToElement(personalDetails);
			 * 
			 * actions.click().moveToElement(basicDetails);
			 * 
			 */

			/*
			 * String file12 ="C:\\Users\\HARISH\\Desktop\\value.jpg";
			 * 
			 * farmerPIC.sendKeys(file12);
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		OnBoardFarmer.click();
		
		
		
		/*
		 * driver.get("https://dev.agrosperity.com/farmer/list");
		 * driver.findElement(By.xpath(
		 * "//body/kivi-app-root[1]/kivi-fullpage-layout[1]/div[2]/div[2]/div[1]/kivi-onboarded-farmer[1]/div[1]/div[4]/kivi-profile-card[1]/div[1]/div[1]/kivi-edit-farmer-option[1]/div[2]/div[1]/i[1]"
		 * )).click(); driver.findElement(By.xpath(
		 * "//body/kivi-app-root[1]/kivi-fullpage-layout[1]/div[2]/div[2]/div[1]/kivi-onboarded-farmer[1]/div[1]/div[4]/kivi-profile-card[1]/div[1]/div[1]/kivi-edit-farmer-option[1]/div[2]/div[2]/div[1]/div[1]/a[1]"
		 * )).click();
		 * 
		 * 
		 * 
		 * 
		 * driver.findElement(By.xpath("//button[@id='myDropdown']")).click();
		 * 
		 * //driver.findElement(By.xpath("//button[@id='myDropdown']")).click();
		 * driver.findElement(By.xpath("//button[contains(text(),'Insurance Nominee')]")
		 * ).click();
		 */		
		
	}

	public static void audit(String appName, Object data) {
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(data);
            //String loggerUrl="";
            URL url = new URL(loggerurl +" " + appName);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            OutputStream os = httpCon.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(json.trim());
            osw.flush();
            osw.close();
            os.close(); // don't forget to close the OutputStream
            httpCon.connect();
            String result;
            BufferedInputStream bis = new BufferedInputStream(httpCon.getInputStream());
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            int result2 = bis.read();
            while (result2 != -1) {
                buf.write((byte) result2);
                result2 = bis.read();
            }
            result = buf.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }	
	/*
	 * public void setUserName(String uname) { txtUserName.sendKeys(uname); }
	 * 
	 * public void setPassword(String pwd) { txtPassword.sendKeys(pwd); }
	 * 
	 * 
	 * public void clickSubmit() { btnLogin.click(); }
	 * 
	 * public void clickLogout() { lnkLogout.click(); }
	 * 
	 */


}
