package pageobjects.kiviapp;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Transport_Page_Objects extends Common_Functions {

	
	@FindBy(xpath = "//*[@id='vehiclesForm']/div/div[1]/label")
	public static WebElement Vechicles;

	@FindBy(id = "isTractorRequired false")
	public static WebElement TractorRequirement;
	
	@FindBy(id = "leaseOut false")
	public static WebElement LeaseOut;
	
	@FindBy(id = "tractorSource Own")
	public static WebElement TractorSource;
	
	@FindBy(id = "tractorDaysPerYear")
	public static WebElement tractorDaysPerYear;
	
	@FindBy(id = "tractorLeasePriceForDay")
	public static WebElement tractorLeasePriceForDay;
	
	@FindBy(xpath = "//*[@id='ownFarmMachineryForm']/div/select")
	public static WebElement OtherFarmMachinery;
	
	@FindBy(id = "machineryOwnership Own")
	public static WebElement machineryOwnership;
	
	@FindBy(xpath = "//button[normalize-space()='Save and Continue']")
	public static WebElement transportsaveButtonClick;

	public Transport_Page_Objects() {
		PageFactory.initElements(driver, this);
	}

	public void setTransportfields(String tractorDaysPerYearDetails, 
			String tractorLeasePriceForDayDetails) {
		Vechicles.click();
		
		TractorRequirement.click();
		
		LeaseOut.click();
		
		TractorSource.click();
		
		tractorDaysPerYear.sendKeys(tractorDaysPerYearDetails);
		
		tractorLeasePriceForDay.sendKeys(tractorLeasePriceForDayDetails);
		
		Select select19 = new Select(OtherFarmMachinery);
		select19.selectByValue("rotavator");
		
		machineryOwnership.click();
		
		transportsaveButtonClick.click();
	}
}

