package pageobjects.kiviapp;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testcases.kiviapp.Common_Functions;

public class Bank_Page_Objects extends Common_Functions {

	@FindBy(id = "bankIfsc")
	public static WebElement bankIFSC;

	@FindBy(id = "bankName")
	public static WebElement bankName;
	
	@FindBy(id = "bankBranch")
	public static WebElement bankBranch;
	
	@FindBy(id = "bankAccountNo")
	public static WebElement bankAccountNo;
	
	@FindBy(id = "confirmBankAccountNo")
	public static WebElement confirmBankAccountNo;
	
	@FindBy(id = "isAccountActive true")
	public static WebElement AccountActive;
	
	@FindBy(id = "Bank Passbook Photo upload")
	public static WebElement BankPassbookPhotoupload;
	
	@FindBy(id = "Bank Upload Doc upload")
	public static WebElement BankUploadDocupload;
	
	@FindBy(xpath = "//button[normalize-space()='Save and Continue']")
	public static WebElement banksaveButtonClick;

	public Bank_Page_Objects() {
		PageFactory.initElements(driver, this);
	}

	public void setBankfields(String bankIFSCNo, 
			String bankAccountNoDetails, String confirmBankAccountNoDetails)
			throws AWTException {
		
		try {
			String file78 = "C:\\Users\\HARISH\\Desktop\\value.jpg";
			
			explicitMethodwait(BankPassbookPhotoupload);
			
			BankPassbookPhotoupload.sendKeys(file78);
			windowScrollmethoddownevent();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		bankIFSC.sendKeys(bankIFSCNo);
		
		/*
		 * Robot robot2 = new Robot(); robot2.keyPress(10); robot2.keyRelease(10);
		 */		
		bankAccountNo.sendKeys(bankAccountNoDetails);
		
		confirmBankAccountNo.sendKeys(confirmBankAccountNoDetails);
		
		AccountActive.click();
		
		banksaveButtonClick.click();
	}
}
	
	

