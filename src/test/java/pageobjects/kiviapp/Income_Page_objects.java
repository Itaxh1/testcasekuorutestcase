package pageobjects.kiviapp;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Income_Page_objects extends Common_Functions {

	@FindBy(id = "sourceOfIncome-0")
	public static WebElement Farming;
	
	@FindBy(id = "sourceOfIncome-2")
	public static WebElement DiaryIncome;
	
	@FindBy(xpath = "//*[@id='cropType1Form']/div/select")
	public static WebElement cropType;
	
	@FindBy(id = "area1")
	public static WebElement Area;
	
	@FindBy(id = "productivityPerAcre1")
	public static WebElement productivityPerAcre1;
	
	@FindBy(id = "noOfSeasons1")
	public static WebElement noOfSeasons1;
	
	@FindBy(id = "avgSalesPrice1")
	public static WebElement avgSalesPrice1;
	
	@FindBy(id = "inputExpenses1")
	public static WebElement inputExpenses1;
	
	@FindBy(id = "harvestingLabourCost1")
	public static WebElement harvestingLabourCost1;
	
	@FindBy(xpath = "//*[@id='cattles1Form']/div/select")
	public static WebElement SelectCattle;
	
	@FindBy(id = "noOfCattle1")
	public static WebElement noOfCattle1;
	
	@FindBy(id = "noOfMilchCattle1")
	public static WebElement noOfMilchCattle1;
	
	@FindBy(id = "avgDailyMilkProduced1")
	public static WebElement avgDailyMilkProduced1;
	
	@FindBy(id = "soldQuantity1")
	public static WebElement soldQuantity1;
	
	@FindBy(id = "pricePerLtr1")
	public static WebElement pricePerLtr1;
	
	@FindBy(xpath = "//*[@id='milkSoldAt1Form']/div/select")
	public static WebElement milkSoldAt1Form;
	
	@FindBy(id = "feedCostPerMonth1")
	public static WebElement feedCostPerMonth1;
	
	@FindBy(id = "groceryExpensesPerMonth")
	public static WebElement groceryExpensesPerMonth;
	
	@FindBy(id = "utilityBills")
	public static WebElement utilityBills;
	
	@FindBy(id = "educationalExpensesPerMonth")
	public static WebElement educationalExpensesPerMonth;
	
	@FindBy(id = "medicalExpensesPerMonth")
	public static WebElement medicalExpensesPerMonth;
	
	@FindBy(id = "rentPainPerMonth")
	public static WebElement rentPainPerMonth;
	
	@FindBy(id = "habitualExpensesPerMonth")
	public static WebElement habitualExpensesPerMonth;
	
	@FindBy(xpath = "//span[normalize-space()='Farming-1']")
	public static WebElement farming1;
	
	@FindBy(xpath = "//span[normalize-space()='Dairy-1']")
	public static WebElement diaryfarming1;
	
	@FindBy(xpath = "//*[@id=\"ngb-panel-6-header\"]/button")
	public static WebElement othersExpensesclick;
	
	@FindBy(xpath = "//button[normalize-space()='Save and Continue']")
	public static WebElement incomesaveButtonClick;

	public Income_Page_objects() {
		PageFactory.initElements(driver, this);
	}

	public void setIncomeExpensesfields(String noOfCattle1det,
			String noOfMilchCattle1det, String avgDailyMilkProduced1det, 
			String soldQuantity1det,String pricePerLtr1det, 
			String feedCostPerMonth1det, String groceryExpensesPerMonthdet,
			String utilityBillsdet, String educationalExpensesPerMonthdet, 
			String medicalExpensesPerMonthdet,
			String rentPainPerMonthdet, String habitualExpensesPerMonthdet) {
		
		DiaryIncome.click();
		diaryfarming1.click();
		explicitMethodwait(diaryfarming1);
		
		Select select15 = new Select(SelectCattle);
		select15.selectByValue("cows");
		
		noOfCattle1.sendKeys(noOfCattle1det);
		
		noOfMilchCattle1.sendKeys(noOfMilchCattle1det);
		
		avgDailyMilkProduced1.sendKeys(avgDailyMilkProduced1det);
		
		soldQuantity1.sendKeys(soldQuantity1det);
		
		pricePerLtr1.sendKeys(pricePerLtr1det);
		
		Select select16 = new Select(milkSoldAt1Form);
		select16.selectByValue("market");
		
		feedCostPerMonth1.sendKeys(feedCostPerMonth1det);
		windowScrollmethodupevent();
		
		explicitMethodwait(diaryfarming1);
		
		diaryfarming1.click();
		
		explicitMethodwait(othersExpensesclick);
		
		othersExpensesclick.click();
		
		groceryExpensesPerMonth.sendKeys(groceryExpensesPerMonthdet);
		
		utilityBills.sendKeys(utilityBillsdet);
		
		educationalExpensesPerMonth.sendKeys(educationalExpensesPerMonthdet);
		
		medicalExpensesPerMonth.sendKeys(medicalExpensesPerMonthdet);
		
		rentPainPerMonth.sendKeys(rentPainPerMonthdet);
		
		habitualExpensesPerMonth.sendKeys(habitualExpensesPerMonthdet);
		
		windowScrollmethodupevent();
		
		explicitMethodwait(othersExpensesclick);
		
		othersExpensesclick.click();
		
		incomesaveButtonClick.click();
	}
	
}
	
	

