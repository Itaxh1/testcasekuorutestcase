package pageobjects.kiviapp;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import testcases.kiviapp.Common_Functions;

public class Insurance_Page_Objetcs extends Common_Functions  {

	
	@FindBy(xpath = "//*[@id='nomineeRelationForm']/div/select")
	public static WebElement nomineeRelation;
	
	@FindBy(id = "nomineeName")
	public static WebElement nomineeName;
	
	@FindBy(name = "nomineeDob")
	public static WebElement nomineeDOB;
	
	@FindBy(xpath = "//button[normalize-space()='Save and Continue']")
	public static WebElement insurancesaveButtonClick;

	public Insurance_Page_Objetcs() {
		PageFactory.initElements(driver, this);
	}

	public void setInsurancefields(String nomineeNameDetails, String nomineeDOBDetails) {

		Select select17 = new Select(nomineeRelation);
		select17.selectByValue("Son");
		
		nomineeName.sendKeys(nomineeNameDetails);
		
		nomineeDOB.sendKeys(nomineeDOBDetails + Keys.ENTER);
		
		insurancesaveButtonClick.click();
	}
}

